# JupyterLite Demo

JupyterLite deployed as a static site to GitLab Pages, for demo purposes.

## ✨ Try it in your browser ✨

➡️ **https://bhugueney.gitlab.io/jupyterlite-template/**

<!-- ![GitLab-pages](https://user-images.GitLabusercontent.com/591645/120649478-18258400-c47d-11eb-80e5-185e52ff2702.gif) -->

Lien vers un Notebook en mode _retro_ : https://bhugueney.gitlab.io/jupyterlite-template/retro/notebooks/?path=Untitled5.ipynb

## Upstream

This site is a clone of jupyterlite github pages demo <https://github.com/bhugueney/jupyterlite-demo/>..

See `.gitlab-ci.yml` for more details, this CI script clone github demo and then
builds jupyterlite site with its contents and then move it to the `public`
folder to host it on gitlab pages.
